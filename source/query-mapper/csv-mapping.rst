CSV datasource mapping
======================

This mapping enables to connect with datasources specified in CSV format. The platform
scans the CSV file an creates an relational schema that enables to perform SQL queries.

The following code shows the general format of a mapping which connects with a CSV
datasource.

.. code-block:: json

   {
      "mapping": "csv",
      "uri": "someuri",
      "key": "someattr",
      "delimiter": ";",
      "refresh": 1000
   }

mapping
   This attribute must contain the value ``csv``.

uri
   The uri of the CSV file to be used as the datasource. Files can be referenced
   using local system routes.

key
   Selects the attribute from the set of mapped objects to be used as the primary key for
   the main table. If the objects do not contain an attribute which can be used as the
   primary key, it is possible to ask the mapper to generate an ad hoc identifier using
   the "_id" value for this attribute.

delimiter
   Enables to select the delimiter used in the mapper file (e.g. ',', ';', '\t')

refresh
   Some CSV datasources can change periodically. This attribute defines the interval to
   retrieve the new data and update the internal structures.

The relational tables are generated in the same way explained for JSON datasources, see
section :ref:`json_mapping_example` for more information about this procedure.
