User local datasets
-------------------

Users can create and register their own datasource by providing a description of the
required dataset schema. This mapping is specified using the following structure. The
mapping type must be set to ``json_schema``, which instructs the platform to process the
schema and create a local storage which can be used by the user to save application
generated data.

The user can provide permissions for the new created dataset using the procedure explained
in :ref:`permissions`.

The platform will use the provided schema to create a compatible storage using the provided
information as a data exmaple.

For example, in this case, it will create an storage with two tables called ``table1`` and
``table2``. The first table will contain three columns with names ``key1``, ``key2`` and
``key3``, being the first one of string type, the second one of integer type and creating
a datetime format for the last.

.. code-block:: json

    {
       "mapping":"json_schema",
       "schema":{
          "tables":[
             {
                "key":"key1",
                "name":"table1",
                "table1":[
                   {
                      "key1": "value1",
                      "key2": 10,
                      "key3": "2015-01-01"
                   }
                ]
             },
             {
                "key":"key2",
                "name":"table2",
                "table2":[
                   {
                      "key1": "value1",
                      "key2": "value2",
                      "key3": 5
                   }
                ]
             }
          ]
       }
    }

Creating a user dataset
^^^^^^^^^^^^^^^^^^^^^^^
The following example shows the code necessary to create a user dataset using
the methods provided by the API. First, user needs to describe the dataset
by providing its basic properties: ``name``, ``description`` and ``jsonMapping``.
As explained in section :doc:`../api-guide/entity` the name is the identifier of
the dataset and, therefore, it must be unique. On the other hand, the description
field enables to provide a long human readable text for the dataset. Finally,
the ``jsonMapping`` allows to specify how the dataset is mapped and the
permissions for each relational table. The mapping mechanism is explained in
section :ref:`mappings`.

The following JSON describes a dataset that is used to store user comments in
the IES Cities platform. In this case, it creates two different tables and
assign permissions to select, insert, update and delete to the ``test_user``,
which must exist in the platform.

.. code-block:: json

    {
       "name":"Test",
       "description":"Testing dataset",
       "jsonMapping":{
          "mapping":"json_schema",
          "schema":{
             "tables":[
                {
                   "key":"id",
                   "name":"Comments",
                   "Comments":[
                      {
                         "id":1,
                         "text":"some_string",
                         "author":"some_string",
                         "rating":1,
                         "app":"some_string",
                         "date":"2015-01-01"
                      }
                   ]
                }
             ],
             "permissions":{
                "insert":[
                   {
                      "table":"Comments",
                      "access":"USER",
                      "users": [
    		      "test_user"
    		 ]
                   }

                ],
                "select":[
                   {
                      "table":"Comments",
                      "access":"USER",
                      "users":[
                         "test_user"
                      ]
                   }
                ],
                "update":[
                   {
                      "table":"Comments",
                      "access":"USER",
                      "users":[
                         "test_user"
                      ]
                   }
                ]
             }
          }
       }
    }


The dataset can be registered using the method explained in section
:ref:`datasets`.

Managing data
^^^^^^^^^^^^^
Data can inserted into or queried from user datasets using the methods explained
in :ref:`data_api`. In this example, only the user `test_user` has permissions
to perform these operations on the dataset. For more information about
permissions see :ref:`permissions`.

Updating and deleting user datasets
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
User dataset properties belong to the user that inserted it and can only be
modified or deleted by the owner or an administrator. Methods regarding dataset
update or deletion are explained in :ref:`datasets`.
