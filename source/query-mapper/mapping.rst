
.. _mappings:

Mapping data sources
====================

Datasets registered in the IES Cities platform, as explained in section :ref:`datasets`,
require a mapping description in order to be connected with the datasources. Mappings are
described in JSON and can have different attributes depending on the type of the dataset
being connected. 

However, all JSON descriptions must contain the mapping attribute which defines the type
of the connected datasource. Currently, the supported datasources are: *database*, *sparql*,
*json* and *csv*. There is also the possiblity to create user dataset by specifying the
schema of the required storage. For more information see :doc:`user-mapping`.

For example, the following code starts the definition of a mapping which connects with a
relational database, and contains other type specific attributes that are defined and
explained in the corresponding sections.

.. code-block:: json

   {
      "mapping": "database",
      "attrA": "value1",
      "attrB": "value2",
      "permissions": {

      }
   }


The following sections define the specific mapping attributes for each datasource type.

.. toctree::

   sparql-mapping
   relational-mapping
   json-mapping
   csv-mapping
   user-mapping

.. _permissions:

Permissions
-----------

Access control is implemented with the usage of permissions that can be configured for
each table provided by the relational view of a dataset.

.. code-block:: json

   {
      "mapping": "database",
      "attrA": "value1",
      "attrB": "value2",
      "permissions" : {
         "select":   [
                        {
                           "table": "table_name1",
                           "access": "ALL"
                        },
                        {
                           "table": "table_name3",
                           "access": "NONE"
                        }
                  ],
         "delete":   [
                        {
                           "table": "table_name1",
                           "access": "ALL"
                        }
                  ],
         "insert":   [
                        {
                           "table": "table_name2",
                           "access": "ALL"
                        },
                        {
                           "table": "table_name4",
                           "access": "USER",
                           "users": ["user1", "user2", "user3", "user4"]
                        }
                  ],
         "update":   [
                        {
                           "table": "table_name1",
                           "access": "OWNER"
                        }
                  ]
      }
   }

As seen before, there are four possible operations: ``select``, ``delete``,
``insert`` and ``update``. Each of the operations can contain a list of access privileges
for different tables. Currently, there are three access privileges supported:

ALL
   Any user can perform the corresponding operation on the specified table.

NONE
   None of the users can perform the corresponding operation on the specified table.

USER
   Only those users specified in the ``users`` can perform the corresponding operation
   on the specified table. Users contained in the list must be valid user names registered
   into the platform.

OWNER
   Only the owner of each row of the table can select or update it. The owner of the row
   is the user who inserted that row in the first place. This permission is the one applied
   by default in the case of JSON (:doc:`json-mapping`) and CSV (:doc:`csv-mapping`)
   datasources that do not have an explicit permission section in their mapping. 