Example
=======

For this example we have used the Majadahonda's event dataset provided at
https://iescities.com/events. This dataset has been used to show the query mapperf
functionality to access an SPARQL dataset using SQL as the query language. The data can be
visually inspected accesing the URL and using the Pubby interface.

.. image:: images/d2rq-events.png

In addition it also provides an SPARQL endpoint (https://iescities.com/events/sparql) that
enables to query the data using the SPARQL query language. An interactive query interface
using SNORQL can be accesed at https://iescities.com/events/snorql/.

.. image:: images/snorql-events.png

The RDF data is directly served by the D2RQ endpoint as read only and with the possible
limitations of this, however, it could be translated to RDF using the tools that D2RQ
provides and be served using other tools such as OpenLink Virtuoso
(http://virtuoso.openlinksw.com).

Registered dataset
------------------

This dataset is registered in the IES Cities platform instance located at 
https://iescities.com with the the following description

.. code-block:: json

	{
		"councilId": 220,
		"datasetId": 220,
		"description": "Majadahonda's Events datasets using SPARQL endpoint",
		"name": "SPARQL Majadahonda's Events",
		"numberAppsUsingIt": 0,
		"publishingTimestamp": "2015-01-30T11:35:35.633Z",
		"quality": 0,
		"qualityVerifiedBy": -1,
		"readAccessNumber": 0,
		"userId": 2536,
		"writeAccessNumber": 0
	}

Schema mapping
--------------

The following code is an excerpt of the mapping configuration file which maps the SPARQL
endpoint to allow the execution of SQL queries.  In order to maintain the compatibility,
the columns has been mapped using the names contained in the original relational database.
However, it could be possible to register multiple maps, as different datasets, for the
same SPARQL endpoint. Each of these datasets can provide a different view of the SPARQL
dataset, make possible to add or hide some tables/columns and change the name of the
mapping column.

.. code-block:: json

	{  
	   "mapping":"sparql",
	   "queryEndpoint":"https://iescities.com/events/sparql",
	   "graph":"",
	   "tables":[  
	      {  
	         "name":"DIRECCIONES_FAVORITAS",
	         "rdfClass":"https://iescities.com/events/resource/FavAddress",
	         "primaryKey":[  
	            {  
	               "name":"id"
	            }
	         ],
	         "columns":[  
	            {  
	               "name":"NOMBRE",
	               "rdfProperty":"https://iescities.com/events/resource/name",
	               "type":"STRING"
	            },
	            {  
	               "name":"LONG",
	               "rdfProperty":"http://www.w3.org/2003/01/geo/wgs84_pos#long",
	               "type":"STRING"
	            },
	            {  
	               "name":"_ID",
	               "rdfProperty":"https://iescities.com/events/resource/id",
	               "type":"STRING"
	            },
	            {  
	               "name":"COORDENADAS",
	               "rdfProperty":"https://iescities.com/events/resource/coordinates",
	               "type":"STRING"
	            },
	            {  
	               "name":"LAT",
	               "rdfProperty":"http://www.w3.org/2003/01/geo/wgs84_pos#lat",
	               "type":"STRING"
	            }
	         ]
	      }
	    ]
	}

Querying the dataset
--------------------

The dataset can be queried using the methods provided by the API. For example, the 
following method `/data/query/{datasetId}/sql` can be invoked with the dataset id and a
SQL query to obtain a response in JSON format. 

Call the following method using the Swagger interface located at
https://iescities.com/IESCities/swagger with the following parameters and query

.. image:: images/query-sql.png

After executing the previous query the server should JSON file with the following results

.. image:: images/json-result.png


Results in JSON-LD format
-------------------------

As the dataset is provided by a SPARQL endpoint, the query-mapper allows to obtain the
results in JSON-LD format (http://json-ld.org/). This format is a JSON compatible but
extended with semantic information, which can be ignored if the application is not going
to perform any semantic process. The following figure shows an example of JSON-LD response
generated using the `/data/query/{datasetId}/sql/jsonld` method, which works as the
previous one but returns the data as a JSON-LD.

.. image:: images/query-jsonld.png

.. image:: images/jsonld-result.png