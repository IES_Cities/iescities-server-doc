Relational database mapping
---------------------------

This mapping type is used to connect with relational datasources and redirect SQL queries
to the associated RDBMS.

The following example describes the structure of a mapping file connecting with a
relational database.

.. code-block:: json

   {
      "mapping": "database",
      "dbType": "PostgreSQL",
      "host": "thehost",
      "database": "dbname",
      "user": "someuser",
      "pass": "somepass"
   }

mapping
   This attribute must contain the value ``database``.
   
dbType
   Specifies the vendor of the RDBMS that is connected with this mapping. Supported types
   are ``SQLite``, ``PostgreSQL`` and ``MySQL``.

host
   The IP/name of the machine where the database is located on.

database
   The name of the database. For SQLite database it should the path of the database file.

user
   User with privileges on the database. Read only datasets will require only select
   privileges while performing writes will require to provide update support. 
   Not required for SQLite databases.

pass
   The password for the previous user. Not required for SQLite databases.
