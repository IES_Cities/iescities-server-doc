Query mapper
============

The Query mapper provides the mechanisms to redirect queries to registered datasets and
transform the results to a JSON format which can be used by application developers. 

The mapping process is based on the usage of schema files that describes how to process the
query for each datasource. Section :doc:`mapping` describes the format of the general
mapping file and how each supported data source is mapped through the IES Cities platform.

The Query mapper contains the functionality required to create a relational view for all
mapped datasources, allowing the execution of queries to retrieve the data. 

Currently, the following datasource types are supported by the Query mapper:

Database
   Relational databases managed by a RDBMS can be mapped and accessed through the platform.
   A more detailed explanation of how to map relational databases is provided in section
   :doc:`relational-mapping`.
   
SPARQL
   Datasources accessible as SPARQL enpoints can also be mapped to dataset registed into
   the platform. Queries are transformed and redirected to the corresponding endpoint. 
   Section :doc:`sparql-mapping` provides a description of the mapping format for this
   type of datasources.

JSON
   Any web resource accesible as a JSON file can be also registered through the Query
   mapper. A description of this mapping format is provided in section :doc:`json-mapping`.

CSV
   Any web resource accesible as a CSV file can be also registered through the Query
   mapper. A description of this mapping format is provided in section :doc:`csv-mapping`.   

The functionality of this module is exposed by the IES Cities API. Section :ref:`data_api`
provides a description of the query and updated methods introduced before.

   Querying using SQL
      The query mapper provides methods to query a dataset using the SQL query language.
      The registered dataset must be compatible with these types of queries, which requires
      that the dataset is fully relational or that it is backed by a datasource that can
      be transformed by the Query mapper to a relational view. Supported formats are:
      `database`, `sparql`, `json` and `csv` datasets.

   Querying using SPARQL
      The query mapper also provides support for querying datasets using SPARQL. In this
      case, only datasets mapped to `sparql` datasources can be queried using this language.
      This functionality can be used by more experienced developers to access resources
      in RDF format. 

   Update SQL dataset
      This module also contains functionality to insert or remove data from the registered
      datasets. Currently, only the update of relational `databases` is supported by the
      query mapper.


.. toctree::

   mapping
   mapping-example