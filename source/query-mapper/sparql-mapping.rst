SPARQL endpoint mapping
-----------------------

Datasources provided by an SPARQL endpoint are mapped using the following description.
The goal of this mapping is to provide a relational view of the RDF structure provided by
the endpoint. RDF classes and properties are mapped to tables and columns by connecting
classes and properties to them.

The following code shows an example of the general structure of an SPARQL mapping file.

.. code-block:: json

   {
      "mapping": "sparql", 
      "sparqlEndpoint": "http://domain/sparql",
      "graph": "",
      "tables": [
         {  "name": "table1",
            "rdfClass": "http://domain/table1#Class1",
            "primaryKey": [ 
               {  "name": "id"   }
            ],
            "columns": [
               {  "name": "colNameA",
                  "rdfProperty": "http://domain/table1#PropertyA",
                  "type": "STRING"
               },
               {  "name": "colNameB",
                  "rdfProperty": "http://domain/table1#PropertyB",
                  "type": "STRING"
               },
               {  "name": "colNameC",
                  "rdfProperty": "http://domain/table1#PropertyC",
                  "type": "STRING"
               }
            ]
         },
         {  "name": "table2",
            "rdfClass": "http://domain/table1#Class2",
            "primaryKey": [ 
               {  "name": "id",
                  "references": "table1"
               }
            ],
            "columns": [
               {  "name": "colName1",
                  "rdfProperty": "http://domain/table2#Property1",
                  "type": "STRING"
               },
               {  "name": "colName2",
                  "rdfProperty": "http://domain/table2#Property2",
                  "type": "STRING"
               },
               {  "name": "colName3",
                  "rdfProperty": "http://domain/table2#Property3",
                  "type": "INTEGER"
               }
            ]
         },
         {  "name": "table3",
            "rdfClass": "http://domain/table1#Class3",
            "primaryKey": [ 
               {  "name": "id"   }
            ],
            "columns": [
               {  "name": "colName1",
                  "rdfProperty": "http://domain/table3#Property1",
                  "type": "STRING"
               },
               {  "name": "colName2",
                  "rdfProperty": "http://domain/table3#Property2",
                  "type": "STRING"
               },
               {  "name": "colName3",
                  "rdfProperty": "http://domain/table3#Property3",
                  "type": "STRING"
               }
            ]
         }
      ]
   }
   
Every schema mapping contains the URL of the SPARQL endpoint and the name of the RDF graph
that it is being translated to a relational view. The graph name can be left empty, meaning
that the default graph for that endpoint will be used. The mapping also contains a list of 
*Table* description elements, one for each relational table that will be created in
the resulting view. 

Each *Table* description contains the following elements: 

Name
   This is the name that SQL queries will use when referring to the table and, 
   therefore, it must be unique inside a mapping. It can also have the special value 
   *manytomany* which is used to specify a N to M relationship mapping table.

RDF Class
   The URI of the RDF class that is mapped to this table. All instances from the RDF model
   belonging to the class will appear as rows of the table.

Columns
 A list of column descriptions which contains information about how class properties are 
 mapped to table attributes.

Primary keys
   A list which defines the name of the primary keys of the table.

Each *Column* description contains the following elements needed by SQLify to map the
RDF properties into attributes:

Name
   The name that SQL queries will use when referring to the column. It must be unique
   inside each table description.

RDF Property 
   The URI of the property that this column is mapping from the RDF model.

Type
   The type of the mapped column, which can be one of the following basic types: ``BOOLEAN``,
   ``CHAR``, ``BYTE``, ``FLOAT``, ``DOUBLE``, ``SHORT``, ``INT``, ``LONG``, ``STRING``,
   when mapping *RDF Data Object* properties. It can also have the special value
   *objectproperty* to indicate that the mapped RDF property is an *Object Property*.

There are more examples in the *IES_Cities-server/query-mapper/src/test/resources/schema/*
directory.

.. note::

   The Query mapper contains an utility to automatically extract SPARQL mappings from an
   endpoint. See the README.md file contained in the source code of query-mapper module of
   the IES Cities platform for information about how to use this utility.