Social API
==========

First implementation of the service for the collection of social data from Facebook and Twitter.

.. _socialapi_configuration:

Social API Configuration
------------------------

To be able to query Facebook and Twitter, some parameters must be configured
inside the ``iescities.properties`` file. For more information see
:ref:`deploying_war`.

For Facebook:
 | fb.clientId
 | fb.clientSecret

are obtained from a Facebook app
https://developers.facebook.com/apps

For Twitter
 | tw.consumerKey
 | tw.consumerSecret
 | tw.accessToken
 | tw.accessTokenSecret

are obtained in a similar way from a Twitter application
https://dev.twitter.com/apps

In iescities.properties other two parameters are found that are used to define the default value for some query parameters
default.facebookSources
default.maxPostsNumber
default.radius
For their meaning, see "Usage"

Usage
-----

The web application exposes various GET methods to get social data:

1. /socialwrapper/posts

2. /socialwrapper/media

3. /socialwrapper/events

The former two methods returns objects of type Post, while the latter one returns objects of type Event.

All of them require request parameters for the query (in JSON format):

 * keyword: one or more words to search for.
 * coordinates: a pair latitude/longitude to restrict the search to an area.
 * radius: the radius the searches are limited to
 * facebookSources: the number of Facebook Places to get posts from. Note that some Place could have no posts.
 * maxPostsNumber: the maximum number of posts per source (Facebook/Twitter). Note that for Facebook, each Place will have associated at most maxPostsNumber / facebookSources

So for example we could have a request for posts

.. code-block:: java

	/socialwrapper/posts?keyword=Trento&coordinates=46.07466,11.11914&radius=10&facebookSources=5&maxPostsNumber=10

with the corresponding response (see the class eu.iescities.server.datawrapper.model.Post and eu.iescities.server.datawrapper.model.Event for the various fields meanings):

	.. code-block:: json

		{
				"Facebook": [
						{
								"parent": "Trento",
								"message": "Amici, siete mai stati ai MERCATINI DI NATALE a Trento ?\r\n\r\n\r\nLa nostra proposta Mercatini di natale: http://bit.ly/offerta-mercatini",
								"source": "Facebook",
								"media": [
										"http://www.facebook.com/photo.php?fbid=562218923866151&set=a.309900649097981.74018.244690192285694&type=1"
								],
								"comments": [],
								"createdAt": 1384935942000,
								"authorId": 244690192285694
						},
						{
								"parent": "Cinema Astra Multisala e Osteria",
								"message": "-------------	IN ANTEPRIMA	--------------\n\" A PROPOSITO DI DAVIS \" di Ethan Coen, Joel Coen\nMartedÃƒÂ¬ 4 Febbraio 2014 - Ore: 18.45 e 21.00\nhttp://trovacinema.repubblica.it/film/a-proposito-di-davis/429436\nhttps://www.youtube.com/watch?v=aEfTIekU49M",
								"source": "Facebook",
								"media": [
										"http://www.facebook.com/photo.php?fbid=587121598036698&set=a.179834138765448.46478.159533037462225&type=1&relevant_count=1"
								],
								"comments": [
										"Renato Meo <3 yes yes yes"
								],
								"createdAt": 1391201502000,
								"authorId": 159533037462225
						},
						{
								"parent": "Festival dell'Arte di Trento",
								"message": "per tutti gli artisti... sabato a bolzano, in palio 600 euro!",
								"source": "Facebook",
								"media": [
										"http://www.facebook.com/events/428741273903581/"
								],
								"comments": [],
								"createdAt": 1379002310000,
								"authorId": 100000366002396
						}
				],
				"Twitter": [
						{
								"parent": null,
								"message": "#Trento #today Ancora chiusa la SS42 del Tonale - Rimane chiusa la straav del passo Tonale dopo Vermiglio, a caus... http://t.co/RPH7TnDWZW",
								"source": "Twitter",
								"media": [],
								"comments": null,
								"createdAt": 1391614216000,
								"authorId": 452061176
						},
						{
								"parent": null,
								"message": "RT @trentofestival: Il Trento Film Festival cerca giovani volontari per la 62Ã‚Â° edizione! Se hai fra i 18 e i 30 anni e ti piacerebbe... httÃ¢â‚¬Â¦",
								"source": "Twitter",
								"media": [],
								"comments": null,
								"createdAt": 1391612526000,
								"authorId": 1353799848
						},
						{
								"parent": null,
								"message": "Firmato l'accordo fra Deutsche Bank e Confindustria di Trento per agevolare le imprese ad... http://t.co/RYuhD1Psea",
								"source": "Twitter",
								"media": [],
								"comments": null,
								"createdAt": 1391607456000,
								"authorId": 358937939
						}
				]
		}

4. /socialwrapper/facebookPlaceEvents

which instead require as parameter
 * placeId: facebook id of the place to get events from

For example, the query

.. code-block:: java

      /socialwrapper/facebookPlaceEvents?placeId=361091394000390

will return the following results:

	.. code-block:: json

			[
				{
					"name":"CENTO (Indie rock FI) live @ bOOkique",
					"parent":"",
					"parentId":"361091394000390",
					"source":"Facebook",
					"startTime":1446841800000,
					"location":"Bookique Trento"
				},
				{
					"name":"STRATEGIE DEL RUMORE. Interferenze tra arte, filosofia e underground. Presentazione e performance del libro di Martina Raponi.",
					"parent":"",
					"parentId":"361091394000390",
					"source":"Facebook",
					"startTime":1446752700000,
					"location":"Bookique Trento"
				}
			]

5. /socialwrapper/usage

Returns the number of likes/shares/retweet about a site

 * url: the url to retrieve data about

.. code-block:: java

	/socialwrapper/usage?url=http://www.repubblica.it/esteri/2014/11/09/news/spagna_catalogna_sfida_madrid_urne_aperte_per_referendum_indipendenza-100119208/

will return:

	.. code-block:: json

				{
					"Facebook" : {
						"likes" : 67,
						"shares" : 2898,
						"replies" : 0
					},
					"Twitter" : {
						"shares" : 157
					}
				}

6. /socialwrapper/friendsUsage

Returns the number of likes about a specific page, made by friends of a specific facebook user

 * url: the url to retrieve data about
 * facebookToken: a facebook client token

.. code-block:: java

	/socialwrapper/usage?url=http://www.repubblica.it/esteri/2014/11/09/news/spagna_catalogna_sfida_madrid_urne_aperte_per_referendum_indipendenza-100119208/&facebookToken=...

will return:

  .. code-block:: json

          {
              "Facebook" : {
                  "likes" : 1
              }
          }
