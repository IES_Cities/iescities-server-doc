Introduction
============

The IES Cities Server's API is a REST-like API that exposes the core functionality of the
IES Cities server. This functionality is used, for example, by the platform web interface,
meaning that all tasks can be accessed by any external code. The IES Cities Server's API
is divided into the following main blocks:
  
Entities
  All the operations regarding to basic database entities are managed under this path.

Social
  Operations regarding social media, e.g. search of topics in Facebook or
  Twitter, are located under this path. 

Log
  Funcionality regarding logs and ratings, in order to get statistical information
  about the usage of applications, datasets and the IES Cities platform itself are
  located in this section.

Data
  This section contains the operations that allows extracting and adding information
  from the datasets of the platform. 

API methods are described using `Swagger <https://helloreverb.com/developers/swagger>`_.
You can access an online description of the methods and their input/output parameters
accessing at the ``<address>/IESCities/swagger/`` URL of the deployed server.


.. tip::

  See :doc:`api-reference` for a complete reference of the API methods.


REST conventions
----------------

The API follows the following conventions to organize and name the available actions

GET
  Actions that retrieve information and do not perform any modification in the server.
  They can receive optional parameters to modify the query. For example 
  ``/entities/councils/all`` retrieves all the registered councils.

POST
  Actions that add new entities to the server. They require to receive a body containing
  the posted data, usually in JSON format. Following the REST conventions, ``POSTs`` are performed
  to the entity URL. An example of a ``POST`` method is ``/entities/councils``.
  
PUT
  As a ``POST`` action, they require to send data in the request body, howewer, in this case
  they are used to modify an existent entity, which means that the modifyed object must 
  already exist in the server. ``/entities/councils/{councilid}`` is an example of a ``PUT``
  method.

DELETE
  These actions are used to remove existing entities from the server. They always receive
  the ``id`` of the entity to delete. The method ``/entities/councils/{councilid}`` is
  an example of this type of actions.

Making requests
---------------

.. note::
  
    Some of the following examples require admin access to some methods. In order to
    deploy a local testable instance, follow installation steps explained in the
    :doc:`../adminguide`.

All requests are done by performing HTTP requests to the corresponding URL. For example,
to retrieve the list of all registered datasets the client must perfom a ``GET`` request on
the following URL

  http://localhost:8080/IESCities/api/entities/datasets/all

In this case, after the HTTP method is succesfully executed, it will return a JSON object
containing the list of all of the registered datasets. The following code shows an excerpt
of the returned JSON data.

.. tip::

   There are tools such as `JSONView for Firefox <https://addons.mozilla.org/en-us/firefox/addon/jsonview/>`_
   or `Chrome <https://chrome.google.com/webstore/detail/jsonview/chklaanhfefbnpoihckbnefhakgolnmc>`_
   that enable to see a JSON response automatically formatted.

.. code-block:: json

    [  
       {  
          "councilId":1776,
          "datasetId":2243,
          "description":"This method provides an example of an sparql dataset",
          "name":"sparql_dataset",
          "numberAppsUsingIt":0,
          "publishingTimestamp":"2015-02-05T13:05:53.46+01:00",
          "quality":0,
          "qualityVerifiedBy":-1,
          "readAccessNumber":0,
          "userId":18503,
          "writeAccessNumber":0
       },
       {  
          "councilId":1776,
          "datasetId":2245,
          "description":"This method provides an example of an sql dataset",
          "name":"sql_dataset",
          "numberAppsUsingIt":0,
          "publishingTimestamp":"2015-02-05T13:05:53.481+01:00",
          "quality":0,
          "qualityVerifiedBy":-1,
          "readAccessNumber":0,
          "userId":18503,
          "writeAccessNumber":0
       }
    ]


Optional parameters
  Some API methods receive optional parameters which change their behaviour. For example,
  the previous method can receive and optional parameter that limits the number of retrieved
  datasets.

    http://localhost:8080/IESCities/api/entities/datasets/all?limit=2

Javascript example
  The following code shows an example of a Javascript call to the API made to obtain the list of
  registered datasets, iterate over the returned objects and print the name of each dataset.

  .. code-block:: javascript

    function getHTTPObject() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    }

    xmlHttp = getHTTPObject();

    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
          var obj = JSON.parse(xmlHttp.responseText);
          for (i = 0; i < obj.length; i++) {
            console.log(obj[i].name);
          }
        }
      }
    };

    xmlHttp.open('GET', 'http://localhost:8080/IESCities/api/entities/datasets/all');
    xmlHttp.send();

Handling errors
  The API methods return a status code to specify the result of the call. As defined by
  the HTTP/1.1 standard, the status codes are

    200 - OK 
      The method was correctly executed.
    404 - Not found
      The URL does not refer to an existing method or entity in the API. Check the method
      signature or expected parameter types. If the method URL is constructed using an
      entity identifier, check that the object id exists in the platform.
    403 - Forbidden
      The method requires valid authentication. Add a valid header and check the supplied
      credentials. See :ref:`authentication` for more details about how to provide user
      authentication info.
    415 - Unsupported media type
      The posted data is not provided using the required data type. Check the expected POST
      data type in the method description.
    500 - Internal Server Error
      An unexpected error occurred during method execution. Contact some IES Cities developer 
      for further information ;).

  The following code provides an example of an invalid HTTP method GET that returns a
  ``404 Error`` by calling a inexistent method. 

  .. code-block:: javascript

    function getHTTPObject() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    }

    xmlHttp = getHTTPObject();

    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
          //do something
        } else {
          console.log('Some error ocurred: ' + xmlHttp.status);
          // perform error handling
        }
      }
    };

    xmlHttp.open('GET', 'http://localhost:8080/IESCities/api/entities/datasets/NON_EXISTENT');
    xmlHttp.send();

.. _authentication:

Authentication
--------------

Some API methods require to provide user credentials to be correctly executed. The
authentication is implemented by using Basic HTTP authentication. See more info about this
type of authentication mechanism in this `Wikipedia article <http://en.wikipedia.org/wiki/Basic_access_authentication>`_
and in this document `RFC2617 <https://tools.ietf.org/html/rfc2617>`_.

All methods requiring authentication must be called with a header containing ``user:pass``
encoded using the `Base64 <http://en.wikipedia.org/wiki/Base64>`_ format.

The following code shows an example of how to add Basic Auth to a ``GET`` method using
Javascript. In this case, the example requires a local version of the IES Cities Server
configured with the default admin account. See :ref:`testing_server` for more info about
how to launch a test instance.

.. code-block:: javascript

    function getHTTPObject() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    }

    xmlHttp = getHTTPObject();

    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
          console.log('Authentication working');
          //do something
        } else {
          console.log('Some error ocurred: ' + xmlHttp.status);
          // perform error handling
        }
      }
    };

    xmlHttp.open('GET', 'http://localhost:8080/IESCities/api/auth/test/admin/hello');

    // add basic auth header
    function make_basic_auth(user, password) {
      var tok = user + ':' + password;
      var hash = window.btoa(tok);
      return "Basic " + hash;
    }

    var auth = make_basic_auth('admin', 'secret');
    xmlHttp.setRequestHeader('Authorization', auth);

    xmlHttp.send();


Sending data
------------

Methods marked with ``POST`` or ``PUT`` require to send data to the server with the request.
The following examples requires to launch a local instance of the IES Cities server
configured with the default admin user and password.

The ``POST`` data is converted to JSON using the Javascript ``stringify`` function and sent to
the server as the body of the request.

  .. code-block:: javascript

    function getHTTPObject() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    }

    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        if (xmlHttp.status == 200) {
          console.log('New dataset correctly inserted')
          //do something
        } else if (xmlHttp.status = 400) {
          console.log('Dataset name already registered');
        }
      }
    };

    xmlHttp.open('POST', url);
    
    //set content type to application/json
    xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    
    // add basic auth header
    function make_basic_auth(user, password) {
      var tok = user + ':' + password;
      var hash = window.btoa(tok);
      return "Basic " + hash;
    }

    var auth = make_basic_auth('bob', 'secret');
    xmlHttp.setRequestHeader('Authorization', auth);

    var dataset = {
                    name: "new_dataset",
                    description: "test_dataset",
                    jsonMapping: ''
                  };

    xmlHttp.send(JSON.stringify(dataset));

.. _using_jquery:

I18N support
------------
Applications can be registered using different supported languages. Any
application registration procedure starts by adding the application in one language, which
provides the id of the application which will be user for additional languages. This first
registration must be performed using the ``POST /entities/apps`` and the application info
object must contain a ``lang`` key with the first registered language identifier: ``EN``,
``ES`` or ``IT``.

.. code-block:: json

    {
    "name": "Your opinion matters (Complaints & Suggestions)",
    "description": "The application will allow citizens to send complaints and suggestions about public services, as well as report problems detected in the public space. 
    This will be done via web, or mobile, and it will be possible to include the geographical coordinates of the incident and a photograph. The city council, in return, will inform the citizens about the status of the incident (fixed, on going or pending).",
    "url": "https://play.google.com/store/apps/details?id=es.zgz.apps.complaints",
    "image": "https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2014/May/07/app-zaragoza-complaints-logo-1172407584-4_avatar.png",
    "lang": "EN",
    "version": "1.0.6",
    "termsOfService": "Terms of Service for Complaints & Suggestions",
    "packageName": "es.zgz.apps.complaints",
    "permissions": "INTERNET, WRITE_EXTERNAL_STORAGE, ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION, CAMERA, VIBRATE",
    "councilid": 2986,
    "scopeid": 3999,
    "tags": [
      "zaragoza",
      "opinion",
      "suggestions",
      "complaints",
      "photo",
      "smart city",
      "311"
    ]
  }

Descriptions in other languages can be added for a registered application using the
``PUT /entities/apps/{appId}``, which requires the application to be already registered,
and providing the new descriptions with the new associated language identifier.

.. code-block:: json

  {
    "name": "Tu opinión importa (Quejas & Sugerencias)",
    "description": "Esta applicación permite a los ciudadanos enviar quejas y sugerencias sobre servicios públicos,
    así como informar sobre problemas detectados en un espacio público.",
    "lang": "ES",
    "termsOfService": "Terminos del servicio de Quejas y Sugerencias",
  }

Methods retrieve application descriptions contain an optional query parameter lang that
can be used to retrieve the information about applications in an specific language. If the
parameter is not supplied the methods using the language feature will retrieve the ``EN``
registered description of the applications.

For example, the following call will try to retrieve all applications with in their italian
description.

  http://localhost:8080/IESCities/api/entities/apps/all?offset=0&limit=1000&lang=IT

If an application does not have description registered in the required language, methods
will fallback to the default available description.

Using jQuery library
--------------------

Examples in this guide can be simplified by using jQuery libraries which provide simpler
methods to perform petitions and handle result conditions.


.. note::

  See `jQuery <http://jquery.com>`_  for more information about this library and how to
  use the methods it provides. Examples using jQuery can be tested with the
  Chrome Developer Console by loading the library before executing each example.:

    .. code-block:: javascript

      var script= document.createElement('script');
      script.type= 'text/javascript';
      script.src= 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js';
      document.head.appendChild(script);

For example, the code to register a new dataset gets more manageable using the methods
provided by jQuery.

.. code-block:: javascript

    function make_basic_auth(user, password) {
      var tok = user + ':' + password;
      var hash = window.btoa(tok);
      return "Basic " + hash;
    }

    API_BASE = 'http://localhost:8080/IESCities/api';

    var dataset =   {   
                      name: "another_dataset",
                      description: "test_dataset",
                      jsonMapping: ''
                    };

    var request = $.ajax ({
      url: API_BASE + '/entities/datasets',
      type: "POST",
      data: JSON.stringify(dataset),
      // adding authentication header
      beforeSend: function (xhr) { 
        xhr.setRequestHeader('Authorization', make_basic_auth('bob', 'secret')); 
      },
      contentType: 'application/json',
      });

    request.done(function(json) {
        console.log(JSON.stringify(json));
    });

    request.fail(function( jqXHR, textStatus ) {
      console.log('Dataset name already registered');
    });


