.. _data_api:

Data API
========

This part of the API contains operations to query and update data on registered datasets.

.. note::

    For more information about how to use the following examples see :ref:`using_jquery`.

Each dataset can have different access restrictions that specified using the mechanism
explained in section :ref:`permissions`.

.. _querying_data:

Querying data
-------------

This methods are used to query the data contained in the registered datasets. All methods
require to provide the ``id`` of the registered dataset, which can be obtained using the
dataset methods from the :doc:`entity`.

/data/query/{datasetId}/sql
    This method enables to execute an SQL query on a registered dataset. The
    Javascript code shows howto query a dataset and obtain a JSON response. The code first
    gets the id of the dataset identified by ``sql_dataset2`` and then executes the
    ``SELECT * FROM country;`` query. Query must be sent in ``text/plain`` format.

    .. note::

      When selecting information users can select the source of the information retrieved
      information using the ``?origin=`` query parameter of the method, which supports
      three different values: ``original``, ``user``, ``any``, allowing to retrieve
      datasource original values only, user provided values only, or a response containing
      a merge between the two data origins.

    .. warning::

        This method can be executed on any dataset compatible with a SQL query. All
        dataset backed by a ``database`` or a ``sparql`` endpoint are compatible with this
        kind of query. Use the ``/entities/datasets/{datasetid}/info`` method to obtain
        information about the dataset.

    .. code-block:: javascript

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = 'sql_dataset2';
        var query = 'SELECT * FROM people;';

        $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
            $.ajax ({
                url: API_BASE + '/data/query/' + json[0].datasetId + '/sql',
                type: "POST",
                data: query,
                contentType: 'text/plain',
                }
            ).done(function(json) {
                console.log(JSON.stringify(json));
            });
        });

    An example of the JSON response is shown in the next figure. The JSON response always
    contains the number of retrieved rows in ``count`` and an array of rows with an entry
    for each retrieved row, which contain an entry for each retrieved column. For example,
    the previous query has returned 4 rows with the ``id``, ``born``, ``occupation`` and
    ``name`` columns.

    .. code-block:: json

        {
          "count": 4,
          "rows": [
            {
              "id": "1",
              "born": "1",
              "occupation": "politics",
              "name": "Gandhi"
            },
            {
              "id": "2",
              "born": "2",
              "occupation": "computers",
              "name": "Turing"
            },
            {
              "id": "3",
              "born": "3",
              "occupation": "smartypants",
              "name": "Wittgenstein"
            },
            {
              "id": "4",
              "born": "2",
              "occupation": "detective",
              "name": "Sherlock Holmes"
            }
          ]
        } 

    An SQL query can also be executed against an dataset backed by a ``sparql`` endpoint.
    For example, executing the previous code while changing the dataset and query

    .. code-block:: javascript

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = 'sparql_dataset2';
        var query = 'SELECT * FROM sioc_UserAccount;';

        $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
            $.ajax ({
                url: API_BASE + '/data/query/' + json[0].datasetId + '/sql',
                type: "POST",
                data: query,
                contentType: 'text/plain',
                }
            ).done(function(json) {
                console.log(JSON.stringify(json));
            });
        });

    causes the ``Query mapper`` to transform the SQL query to its SPARQL equivalent, using the
    dataset's associated mapping and execute it against the corresponding endpoint. The
    results of this query are shown in the following JSON code, which follows the same
    response format but uses the names defined in the registered dataset's mapping file.

    .. code-block:: json

        {  
           "count":2,
           "rows":[  
              {  
                 "id":"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare",
                 "foaf_page":"http://www.slideshare.net/uaguiler/",
                 "foaf_accountServiceHomepage":"http://www.slideshare.net/",
                 "sioc_name":"uaguiler",
                 "sioc_id":""
              },
              {  
                 "id":"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin",
                 "foaf_page":"http://www.linkedin.com/pub/unai-aguilera/",
                 "foaf_accountServiceHomepage":"http://www.linkedin.com/",
                 "sioc_name":"",
                 "sioc_id":"26263133"
              }
           ]
        }

/data/query/{datasetId}/sql/jsonld
    This method is similar to the previous one but results are returned in JSON-LD format
    instead of the plain JSON format used before. To obtain more information about the
    JSON-LD standard visit `json-ld.org <http://json-ld.org/>`_.

    .. warning:: 

        As the method needs to extract the semantic description of each value to construct the
        JSON-LD response, the method can only be executed against datasets backed by SPARQL
        endpoints. Use the ``/entities/datasets/{datasetid}/info`` method to obtain information
        about the dataset.

    The following example performes a query on a dataset backed by a SPARQL endoint and
    returns the result in JSON-LD format.

    .. code-block:: javascript

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = 'sparql_dataset2';
        var query = 'SELECT * FROM sioc_UserAccount;';

        $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
            $.ajax ({
                url: API_BASE + '/data/query/' + json[0].datasetId + '/sql/jsonld',
                type: "POST",
                data: query,
                contentType: 'text/plain',
                }
            ).done(function(json) {
                console.log(JSON.stringify(json));
            });
        });

    The JSON-LD result of the previous query is shown in the following code block.

    .. code-block:: json

        {  
           "count":2,
           "rows":[  
              {  
                 "@context":{  
                    "foaf_page":"http://xmlns.com/foaf/0.1/page",
                    "foaf_accountServiceHomepage":"http://xmlns.com/foaf/0.1/accountServiceHomepage",
                    "sioc_name":"http://rdfs.org/sioc/ns#name",
                    "sioc_id":"http://rdfs.org/sioc/ns#id"
                 },
                 "@id":"http://www.morelab.deusto.es/resource/unai-aguilera#slideshare",
                 "foaf_accountServiceHomepage":"http://www.slideshare.net/",
                 "foaf_page":"http://www.slideshare.net/uaguiler/",
                 "sioc_id":"",
                 "sioc_name":"uaguiler"
              },
              {  
                 "@context":{  
                    "foaf_page":"http://xmlns.com/foaf/0.1/page",
                    "foaf_accountServiceHomepage":"http://xmlns.com/foaf/0.1/accountServiceHomepage",
                    "sioc_name":"http://rdfs.org/sioc/ns#name",
                    "sioc_id":"http://rdfs.org/sioc/ns#id"
                 },
                 "@id":"http://www.morelab.deusto.es/resource/unai-aguilera#linkedin",
                 "foaf_accountServiceHomepage":"http://www.linkedin.com/",
                 "foaf_page":"http://www.linkedin.com/pub/unai-aguilera/",
                 "sioc_id":"26263133",
                 "sioc_name":""
              }
           ]
        }

Updating data
-------------

The data contained in the registered datasets can be updated using the following methods.
In the case of datasets mapped to external JSON (:doc:`../query-mapper/json-mapping`) or CSV 
(:doc:`../query-mapper/csv-mapping`), as the source is usually read-only, the data inserted
by the users is stored locally by the platform in a automatically created database. For
more information about how to filter the information depending on its origin see
:ref:`querying_data`.

/data/update/{datasetId}/sql
    This method enables to perform a data update on a registered dataset. The update must
    executed by registered users and authorized users. See :ref:`permissions` for 

    The following code shows an example that executes an update on a SQL backed dataset.

    .. code-block:: javascript

        function make_basic_auth(user, password) {
            var tok = user + ':' + password;
            var hash = btoa(tok);
            return "Basic " + hash;
        }

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = 'sql_dataset2';
        var query = "INSERT INTO people (born, occupation, name) VALUES (3, 'thinking', 'Einstein');";

        $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
            $.ajax ({
                url: API_BASE + '/data/update/' + json[0].datasetId + '/sql',
                type: "POST",
                data: query,
                beforeSend: function (xhr){ 
                    xhr.setRequestHeader('Authorization', make_basic_auth('admin', 'secret')); 
                },
                contentType: 'text/plain',
                }
            ).done(function(json) {
                console.log(JSON.stringify(json));
            });
        });

    The result of the query is a JSON object containing the number of modified rows.

    .. code-block:: json

        {
            "rows": 1
        }

    In addition the method supports the execution of multiple update statements in a single
    transaction. In order to enable transaction support the update method must be invoked
    with the ``?transaction=true`` parameter. This will enable the execution of multiple
    update statements passed in the following form as the method data.

    .. code-block:: none

      insert into people (id, name) values (null, 'name1');
      insert into people (id, name) values (null, 'name2');

    If there is a error during the execution of some of the passed update statements, the
    dataset will perform and automatic rollback to the previous state. If update statements
    are correctly executed, the method will return the number of total modifed rows.

    The following code shows an example that executes multiple update statements inside a
    single transaction.

    .. code-block:: javascript

        function make_basic_auth(user, password) {
            var tok = user + ':' + password;
            var hash = btoa(tok);
            return "Basic " + hash;
        }

        API_BASE = 'http://localhost:8080/IESCities/api';

        var dataset = 'sql_dataset2';
        var query = "insert into people (id, name) values (null, 'test_name'); insert into people (id, name) values (null, 'test_name');";

        $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
            $.ajax ({
                url: API_BASE + '/data/update/' + json[0].datasetId + '/sql?transaction=true',
                type: "POST",
                data: query,
                beforeSend: function (xhr){ 
                    xhr.setRequestHeader('Authorization', make_basic_auth('admin', 'secret')); 
                },
                contentType: 'text/plain',
                }
            ).done(function(json) {
                console.log(JSON.stringify(json));
            });
        });

/data/update/{datasetId}/json
  This methods executes an update specified using a JSON format. The structure of the
  JSON update file is structured into two different sections: first one contains the
  deletes that must be executed against the dataset, while the second one contains the
  new data to insert.

  Each ``insert`` or ``update`` section contains of updated table, each object containing
  the name of the table, a list changes. For the delete block, the ``rows`` contains a
  list of ids to remove from the corresponding table. In the case of the insert block,
  each row is an object containing the new data to insert, using an schema of
  ``"column_name": value``.

  .. code-block:: json

    {
      "deletes": [
        {
          "table": "people",
          "rows": [
            {
              "id": 500
            },
            {
              "id": 501
            }
          ]
        }
      ],
      "inserts": [
        {
          "table": "people",
          "rows": [
            {
              "id": 500,
              "born": 1,
              "invalid": "physicist",
              "name": "Einstein"
            },
            {
              "id": 501,
              "born": 1,
              "invalid": "physicist",
              "name": "Schrodinger"
            }
          ]
        }
      ]
    }


  The following code shows an example of an update performed using the previous JSON
  format.

  .. code-block:: javascript

    function make_basic_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }

    API_BASE = 'http://localhost:8080/IESCities/api';

    var dataset = 'sql_dataset2';
    var update = '{
                    "inserts": [
                      {
                        "table": "people",
                        "rows": [
                          {
                            "id": "null",
                            "born": 1,
                            "occupation": "physicist",
                            "name": "Einstein"
                          },
                          {
                            "id": "null",
                            "born": 1,
                            "occupation": "physicist",
                            "name": "Schrodinger"
                          }
                        ]
                      }
                    ]
                  }';

    $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
        $.ajax ({
            url: API_BASE + '/data/update/' + json[0].datasetId + '/json',
            type: "POST",
            data: update,
            beforeSend: function (xhr){ 
                xhr.setRequestHeader('Authorization', make_basic_auth('admin', 'secret')); 
            },
            contentType: 'application/json',
            }
        ).done(function(json) {
            console.log(JSON.stringify(json));
        });
    });

  As the previous method, the correct execution of this method returns the number of
  modified rows.

Miscellanous methods
--------------------

This section contains a description of those methods related with data management but that
are not directly related with querying or updating datasets.

/data/query/{datasetId}/transform/sql
    This method allows to transform an SQL query to its SPARQL equivalent using the
    mapping associated with the provided dataset identifier. The resulting SPARQL query is
    not executed but only returned to the user in ``text/plain`` format.

       .. code-block:: javascript

            API_BASE = 'http://localhost:8080/IESCities/api';

            var dataset = 'sparql_dataset2';
            var query = 'select * from sioc_UserAccount;';

            $.getJSON(API_BASE + '/entities/datasets/search?keywords=' + dataset, function(json) {
                $.ajax ({
                    url: API_BASE + '/data/query/' + json[0].datasetId + '/transform/sql',
                    type: "POST",
                    data: query,
                    contentType: 'text/plain',
                    }
                ).done(function(response) {
                    console.log(response);
                });
            });

    The response to the previous transformation is the following query in SPARQL format.

    .. code-block:: sparql

        PREFIX  xsd:  <http://www.w3.org/2001/XMLSchema#>
        select (str(?a) as ?id) (str(?b) as ?foaf_accountServiceHomepage) (str(?c) as ?foaf_page) (str(?d) as ?sioc_id) (str(?e) as ?sioc_name) where {
            ?a  a   <http://rdfs.org/sioc/ns#UserAccount> .
            optional {?a    <http://xmlns.com/foaf/0.1/accountServiceHomepage>  ?b .}
            optional {?a    <http://xmlns.com/foaf/0.1/page>    ?c .}
            optional {?a    <http://rdfs.org/sioc/ns#id>    ?d .}
            optional {?a    <http://rdfs.org/sioc/ns#name>  ?e .}
        }

/data/query/kpi
    This method enables to execute queries on the internal server's database. The queries
    must return a single ``integer`` value. It is used for statistic purposes only.


User data storage
-----------------

Data inserted by users is managed differently depending on the type of datasource that
is connected with the updated dataset. See :ref:`mappings` for more details about
supported mapping types.

Database
  In the case of a relational database, user updates are executed agains the connected
  datasource and perform direct updates on the provided data. Administrators configuring
  and connecting external datasources backed by a relational database should apply the
  correct permissions using the DBMS own mechanism and/or the functionality provided by
  the platform and explained in :ref:`permissions`.

JSON, CSV
  External datasources in JSON or CSV format cannot be directly updated as they are
  generally provided as read only files. In this case, the data inserted by the users is
  added to an specifically created local database created and managed by the platform.
  When retrieving data, the original data obtained from the JSON or CSV datasource and the
  data provided by users is merged and returned as it was provided by a single datasource.
  The user data is stored in a local database. See :ref:`backup_data` for more information
  about where is this data stored.

SPARQL
  In this case, updates are propagated to the external SPARQL endpoint, which should
  provide update support in order to execute the resulting SPARUL queries.

User storage
  Users can create storage datasets for their applications providing an schema definition,
  as explained by :doc:`../query-mapper/user-mapping`. In this case, the data provided by the users is
  stored in a local database. See :ref:`backup_data` for more information
  about where is this data stored.