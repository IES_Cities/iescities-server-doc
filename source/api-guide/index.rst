API guide
=========

This section documents the IES Cities Server's API, which can be used by developers to
write applications and code that interacts with a server instance and its data.

.. toctree::

    intro  
    entity
    data
    logging
    social
    api-reference