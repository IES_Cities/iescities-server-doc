Overview
========

:doc:`userguide`
	The guide for users of the IES Cities plataform that explains how to use the web
	interface and common concepts.

:doc:`adminguide`
	This guide contains information to install an maintain a IES Cities platform instance.

:doc:`api-guide/index`
	For developers, this guide explains how to use the API and its data.

:doc:`query-mapper/index`
	This section contains a description of the *Query Mapper* module and some working
	examples.