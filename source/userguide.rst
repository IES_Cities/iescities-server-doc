User guide
==========

This section documents and explains in detail how to use the IES Cities web interface and common concepts

What is IES Cities?
-------------------
**IES Cities** is a project partially funded by the European Commission that will adapt and use most of the technical components and achievements coming from previous projects in order to facilitate the use of an open technological platform in different cities across Europe, allowing the citizens to produce and consume internet-based services based on their own and external data related to the cities.

**For whom?**

IES Cities has detected the following groups of stakeholders:

*    **Citizens:** One of the aims of IES Cities is increasing the knowledge of a city by their population as well as increasing the value of the data from it, making it useful for their inhabitants and visitors  around, giving an added value to the existing data and involving the users, in a way that they can be considered at the same time producers and consumers of contents (super-prosumer concept). There will be different types of citizens using the services, like passengers in the transport networks, patients suffering from a certain illness, drivers, carers, students, business people, families, etc.

*    **SMEs:** the open platform in the project, will allow the creation of services benefiting the local businesses. For instance, the owner of a shop or several ones could generate a service to score the quality of their products, offering special discounts according to the citizens? inputs or position etc. The potential impact to foster local businesses, given an open platform of these characteristics, is enormous.

*    **ICT-developing companies:** The IES Cities open platform infrastructure will enable the chance to create new apps and services based on the needs of final users, bringing new possibilities and added value to these users.

*    **Public administration:** the interaction with the users will enable public administrations to improve and foster the use of their deployed sensors in urban areas and open databases.

**How this is going to be done?**

A thorough **evaluation of the pilot phases** is being carried out by combining online surveys completed mainly by citizens and developers with the analysis of the logs gathered during service at the end-users smart phones in order to get feedback from the usage of the application and the services.


Using IES Cities
----------------
The common web entry point to the IES Cities ecosystem is the IES Cities Web interface (https://iescities.com). The main purpose of this web interface is to make public data of the cities involved in the project; namely Bristol (UK), Rovereto (Italy), Zaragoza and Majadahonda (Spain) available to different users in order to foster the creation of new and innovative urban apps. 

Three different kind of users are expected to be users of the IES Cities web interface:

*	Citizens from Bristol (UK), Zaragoza (Spain), Rovereto (Italy) and Majadahonda (Spain) --> Through the Web interface citizens will have access to a set of applications which are available in the city. 

*	Developers --> The IES Cities web interface provides useful information about a set of open datasets that are available through the IES Cities ecosystem. Developers, after registration, are enable to create and 
	publish new apps in the IES Cities ecosystem built on top of the public data that the different city councils made available. 

*	Bristol (UK), Zaragoza (Spain), Rovereto (Italy) and Majadahonda (Spain) city councils --> City councils act as the IES Cities platform managers. Through this interface they will have access to a set of 
	statistics about the platform usage and they will also take the possibility to make public new datasets.

Common use of IES Cities web
----------------------------

In order to access to the IES Cities ecosystem, first of all, a user should type https://www.iescities.com in his/her web browser. 

.. image:: /img/iescitiesweb.png

The official language of IES Cities project is English, thus the web site is shown in English by default. 

.. image:: /img/iescitiesweb_2.png

However, as the project consortium is made up of partners from three different countries, users will be able to browse the web in three different languages: Italian, English and Spanish, by selecting one of the flags located in the upper side of the homepage. 

.. image:: /img/flags.png 
	:align: center

The web main menu is located is located beside the flags. This menu enables access to the following options:

.. image:: /img/forum_icon.png 
	:height: 50px 
	:width: 50px
	
*	By selecting forum icon a web user has access to the IES Cities forum. The IES Cities forum is the tool where IES Cities users may share their doubts and comments. The forum is totally managed by technical people deeply involved in the project who answers and solves potential questions and doubts and who creates conversations with IES Cities users in order to improve the system.

.. image:: /img/iescitiesweb_forum.png

.. image:: /img/help_icon.png
	:height: 50px 
	:width: 50px
	
*	By selecting help icon a sub-menu appears allowing users to:

.. image:: /img/iescitiesweb_help.png

1.	Learn more about IES Cities project

.. image:: /img/iescitiesweb_learnmore.png

2.	Read the terms of use of this site and the remaining IES Cities tools. In that case a new tab is opened where the "Terms and Conditions of use" document is shown.

.. image:: /img/contactUs_icon.png
	:height: 50px 
	:width: 50px

*	By selecting tool icon a sub-menu is showed providing web users the following functionality:

.. image:: /img/iescitiesweb_3.png

1.	IES Cities Support -> A three languages contact form has been specifically designed to allow IES Cities users to report technical problems or to request general / technical information about the IES Cities project. Depending on the city selected the notification will be redirected to the corresponding partner. This partner will analyse the notification and will send it to the project coordinator in case a user is requesting for more information about the project or to the technical support team in case a user is reporting a technical issue. 

.. image:: /img/iescitiesweb_cf.png


2.	Getting started with IES Cities ? Access to the IES Cities user manual which contains all the required information for a correct use of the IES Cities ecosystem. 

.. image:: /img/twitter_icon.png
	:height: 50px 
	:width: 50px
.. image:: /img/facebook_icon.png
	:height: 50px 
	:width: 50px


*	By selecting facebook or twitter a web user has access to the IES Cities Twitter (https://twitter.com/iescities) and Facebook (https://www.facebook.com/IESCities) pages and finally,

.. image:: /img/iesCities_icon.png
	:height: 50px 
	:width: 50px
	
*	By selecting IESCities icon a web user has access to the official project website of the project (http://iescities.eu/) which provides detailed information about the project, e.g. partners, public documents, events and so on. 

Apart from the main menu options, any type of user may access to a set of specific functionalities by clicking the name of the city where s/he is interested in:

.. image:: /img/iescitiesweb_4.png


.. _IES_CITIES_REGISTRATION:

IES Cities Registration
-----------------------
After clicking on the name of the city in the IES Cities web interface homepage, a user that would like to publish an application should first of all create an account. By clicking  Sing up icon a short form (see below) appears allowing a potential developer to create and account choosing the city for which s/he would like to publish applications. 

.. image:: /img/iescities_register.png

The user can link it's register in IESCities with google, for that, the user must click the "Enable access with google button" and accept the google requeriments.

.. image:: /img/iescities_google_requeriment.png

If all goes well, "User granted access" message appears and the name and surname are filled with google data.

.. image:: /img/iescities_google_requeriment_2.png

Of course this is optional, after that the user must complaint the rest of the data (name and surname can be modified) and push "Register" button.



Using IES Cities as a Citizen
-----------------------------

Once a citizen, for example from Zaragoza (Spain), has clicked the name of the city, s/he has access to specific information about the city and its main role within IES Cities project.

.. image:: /img/iescitiesweb_5.png

Below this information about the city, on the central part of the page, several options are offered to the user depending on his/her desired role in the system:

.. image:: /img/iconContact.png
	:height: 37px 
	:width: 140px

*	By clicking  Sing in/Sing out buttton the user is asked to create an account and register into the platform. The registration process is required whenever a user wants to publish and promote new application about his/her city. For more information go to "Using IES Cities as a Developer".

.. image:: /img/googlePlay.png
	:height: 37px 
	:width: 140px

*	By selecting google play button the user is redirected to google play to download in his/her mobile device the IES Cities Player. This application acts as an IES Cities marketplace broker allowing users to discover which applications are available on their surroundings making use of public data.

.. image:: /img/feeedbackIcon.png
	:height: 37px 
	:width: 140px

*	By selecting feedback button a short form is showed to the user with the aim of getting feedback that allow us to improve the platform and the services.   

.. image:: /img/iescitiesweb_left.png
	:width: 200px 
	
.. image:: /img/iescitiesweb_rigth.png
	:width: 200px

In addition to this information, in the left side of the page the list of applications and datasets that are available for this specific city are showed. In the right side, the form for logging into the system is also shown (see figures above).  

By selecting one of the applications, the following screen appears. This screen provides a short description about the main purpose of the selected application and a link to Google play where this application may be downloaded.   

.. image:: /img/iescities_dataset.png


Using IES Cities as a Council Admin
-----------------------------------
IES Cities platform is also devised a tool to enable city admin to manage the registered users in their city, publish and promote new datasets about the city and also they are able to acces to city statistics. 

.. image:: /img/council_admin_menu.png

A city admin user can see the status of the registered user in its city, if is admin or if is a developer. From this menu the council admin is able to view the details of an user and also can delete it.

.. image:: /img/lista_usuarios_council_admin.png

In order to upload a new dataset the council admin will be able to push "new" button in "City Datasets" block.

.. image:: /img/new_dataset.png

Then the council admin must fill all the fields in the form.

.. image:: /img/new_dataset_form.png

The name must be unique, a description of the dataset must be inclued and the mapping of the dataset  (see:  :doc:`query-mapper/index`) also a rating of the dataset is required.

The admin that upload a new dataset is marked as Validator of it.

If the city admin want's to update or delete one f the datasets of it's city, it can be done from the detail view of dataset, the option will appear at the end of dataset and tables description.

.. image:: /img/dataset_botones.png

Using IES Cities as a Developer
-------------------------------

IES Cities platform is also devised a tool to enable users to publish and promote new applications about the city where s/he lives. These kind of users are considered within the IES Cities platform as "developers" since they are able to develop an application using several IES Cities datasets. 

A registered user can login in the platform using its user and password or using "Access with google button", in order to use this, the user must be registered using "Enable access with google button".

Then, a vertical menu appears on the left side of the page (see figure below) providing details about his/her profile and giving the developer the possibility to:

*	Access to "My Applications": In this case a list of the currently published applications is showed to the user.
*	Access to "Change Profile" option where the developer is allowed to update his/her user profile.
*	Reset the current password and,
*	Logging out from the IES Cities platform. 

.. image:: /img/iescities_loguser.png 

How a developer may enable access with google?
----------------------------------------------
In order to access with google, IESCities platform provides a service that validate an user taking as parameter google 'id_token'. 
If a developer wants to logging its user with google using IESCities platform, the first step is that its users will be registered in IESCities using "Enable access with google" option.

See :ref:`IES_CITIES_REGISTRATION` for more info about how to register in IESCities.

.. note::

    In order to login with google your application always need to create a client ID and client secret in google developer console, See: `Google Developer Console <https://console.developers.google.com/project>`_

.. note::
    
	The Web interface use th e Google+ javascrip api: See:  `Javascript Flow <https://developers.google.com/+/web/signin/javascript-flow>`_

Javascript api use 	'gapi.auth.signIn(options);' here you have an example of use.

.. code-block:: javascript

	/**
	For google singing
	*/
	var options = {
		'callback' : loginFinished,
		'approvalprompt' : 'force',
		'clientid' : '<Application clientid>',
		'cookiepolicy' : 'single_host_origin',
		'scope': 'openid profile'
	};
 
	function auth(){   		
		gapi.auth.signIn(options);

	}	

The result of login is processed in callback function 'loginFinished' in this case.
	
.. code-block:: javascript

	var loginFinished = function(authResult) {
		if (authResult) {
			
			if (authResult['status']['signed_in']) {
				gapi.auth.setToken(authResult);
			} 
			goauth = toJsonObject(authResult);
  

		}	 
		iesGooglelogin();
	};

 
	function toJsonObject(authResult){
		var token = new Object();
		token.id_token = authResult['id_token'];
		token.access_token = authResult['access_token'];
		token.state =authResult['state'];

		token.error = authResult['error'];
		token.error_description = authResult['error_description'];
		token.expires_in =authResult['expires_in'];

		token.authUser = authResult['authUser'];
		token.error_description = authResult['error_description'];
		token.expires_in =authResult['expires_in'];
		
		return token;

	}
	
With the 	goauth.id_token value we can call the IESCities 'googlelogin' service in the following way.
	
.. code-block:: javascript
	
	/**
	* Login of user.
	* Gets the current user information.
	* Use as callback procesarResultado function.
	*/
	function iesGooglelogin(){
		url = '/IESCities/api/entities/users/googlelogin';
		xml = getHTTPObject();
		xml.onreadystatechange = procesarResultado;
		var params = 'tokenstring='+goauth.id_token;
		xml.open('GET',url+'?'+params);
  	
  	
		xml.setRequestHeader("Content-type", "application/json");
  	
		xml.send();
		return true;	
	}
	/**
	 * Callback of ieslogin function.
	 */
	function procesarResultado(){
		
		if (xml.readyState == 4){
			if (xml.status == "200" && xml.responseText!=""){
				// Here in xml.responseText variable you have the object User if the login has been correctly procesed.
			}
			else {
				//An error has occurred.
			}
			
		}

	}
	/**
	 * Gets the HttpObject that depends of the browser
	 * @returns
	 */
	function getHTTPObject(){
		if (typeof XMLHttpRequest != 'undefined'){
			return new XMLHttpRequest();
		}
		try{
			return new ActiveXObject("Msxml2.XMLHTTP");
			
		}
		catch (e){
			try{
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				
			}
		}
	}
	
How a developer may create an application?
------------------------------------------


If a user wants to develop an app using IES Cities Datasets, it is necessary to follow some steps:

First of all, a previously registered developer needs to choose the Dataset s/he wants to use from the list of datasets showed on the left side of the page. The field "Description" explains everything about the specific Dataset.


.. image:: /img/dataset.png

When the developer has decided that this Dataset could be useful, then:


It is important to take note of the ID and the tables that we can use of the chosen Dataset.

.. note::
	This ID let us to make Queries and get data from the Dataset

To consult the IES Cities RESTFul API the developer has to access through the links that may be found in the "How to use" section of each Dataset.

.. image:: /img/howtousesection.png

There are two types of Dataset:
*    DataBase Datasets: We use SQL queries to access data
*    RDF Datasets: These Datasets store semantic information and qe we qill access it through SPARQL queries.

In order to get information about how to use the Datasets, we should click on the link "SQL Query". 
Through this, you will get a tool where you can test the services of each dataset.

More detailed infromation:

:doc:`api-guide/index`


How a developer may publish an application?
-------------------------------------------


Once a developer has finally developed his own app using the IES Cities Datasets, it is time to publish the App in the IES Cities system. 

First of all, you should publish your app in an App Market such as Google Play or the App Store.

Once your App is available to be downloaded through an App Market, you have to publish it in the IES Cities Web Page. This part is very straightforward:

*	Log in on your IES Cities Developer account
*	On the left side of the screen, in the 'City Applications' part you can see a 'New' Button. Click it.

.. image:: /img/web_new_app.png
	:align: center

*	A new Application Form appears and you only have to fill it. 

		*	Name of the application: Just the name of your App
		*	Description: Your App is useful for...
		*	Permissions: You can add the System Permissions of your app
		*	URL: Here you have to paste the Market link where everybody can find your app.
		*	Image: An external link of the Icon image of your App
		*	Package Name: The package name of your app (Ej: com.mycompany.citizens)
		*	Version: The number of the las versions of the app
		*	Terms of service: here you can include a licence file or something like that for your app.
		*	Tags: Words to make easier to find your app
		*	Geographical Scope: The city where your app will work for
		*	Used Data: Which dataset have you used in the App
	
*	Then you only have to click on "Create" 
	
How a developer may check his applications?
-------------------------------------------

Once a developer has finally published his App into the IES Cities Service, is able to check his apps clicking on "My Applications" on the right side of the Web

.. image:: /img/web_my_app.png
	:align: center

How a developer may edit/delete an application?
-----------------------------------------------

The Developer can edit or even delete one or more of his apps just clicking on "My Applications" and clicking on "Edit" or "Delete"

.. image:: /img/web_user_apps.png
	:align: center
 






	