Installation guide
==================

Requirements
-------------

The IES Cities Server has the following requirements that must be correctly installed and configured.
Follow the instructions provided for your platform to install them before proceeding.

*	`Java SE Development Kit 8 <http://www.oracle.com/technetwork/java/javase/downloads/index.html>`_
* `Maven 3 <http://maven.apache.org/>`_
*	`PostgreSQL <http://www.postgresql.org/>`__

Download code
-------------

Clone the source code from the Bitbucket repository
(https://bitbucket.org/IES_Cities/ies_cities-server) ::

    git clone https://bitbucket.org/IES_Cities/ies_cities-server.git

Database setup
--------------

PostgreSQL must be correctly installed following the particular instructions for the OS.
This manual explains how to setup the database using the *psql* command line tool.
The default database name user and password can be changed by updating the datanucleus
configuration. See :ref:`datanucleus_configuration` for more details.

Create database
	Execute the following command in the psql client command line to create the *iescities*
	database ::

		psql> CREATE DATABASE iescities;

Add database user
	Add user *iescities* with default password *citiesies* and add it privileges on created
	database ::

		psql> CREATE USER iescities WITH PASSWORD 'citiesies';
		psql> GRANT ALL PRIVILEGES ON DATABASE iescities to iescities;

.. _datanucleus_configuration:

Datanucleus backend configuration
---------------------------------

*This step is optional.*

The DataNucleus database backend needs to be configured. We provide the following example
that defines the default database name, user and password required to connect to the
PostgreSQL database.


**src/main/resources/datanucleus.properties**

.. code-block:: jproperties

	javax.jdo.PersistenceManagerFactoryClass=org.datanucleus.api.jdo.JDOPersistenceManagerFactory
	javax.jdo.option.ConnectionDriverName=org.postgresql.Driver
	javax.jdo.option.ConnectionURL=jdbc:postgresql://localhost:5432/iescities
	javax.jdo.option.ConnectionUserName=iescities
	javax.jdo.option.ConnectionPassword=citiesies
	javax.jdo.option.Mapping=hsql

	datanucleus.autoCreateSchema=true
	datanucleus.validateTables=false
	datanucleus.validateConstraints=false

Build server
------------

The following command will download all required dependencies, execute all automatic tests
and generate a deployable war file called ``IESCities.war`` in the ``rest-interface/target``
directory. The following command must be executed inside the project's main directory. ::

	mvn install

.. note::

	If there are some failing tests the build process will stop and not generate the deployable
	war file. In order to build the code ignoring failing tests run the following command. ::

		mvn install -Dmaven.test.failure.ignore=true

If the build process finishes correctly, a deployable ``IESCities.war`` file can be found
inside the ``rest-interface/target`` directory.

.. warning::

	**Not recommended.** In order to skip tests add the following parameter to the build
	command ::

		mvn install -DskipTests

Performance testing
-------------------

Performance tests can be executed adding the following parameter to the build command ::

	mvn test -DargLine="-Dcontiperf.active=true"

The output report can be found opening the ``target/contiperf-report/index.html`` inside
the corresponding module folder.

.. _testing_server:

Testing the server
------------------

The server can be tested using an embedded Jetty server that automatically starts and
deploys the previously generated war. Inside the ``rest-interface`` directory run ::

	mvn jetty:run

After launching the embedded server, the web methods are described and can be tested by
connecting to the following URL

	http://127.0.0.1:8080/IESCities/swagger/index.html

.. _deploying_war:

Deploying war
-------------

The IES Cities server can be deployed in a servlet container such as
`Apache Tomcat <http:://apache.tomcat.org>`_. If the server was correctly built, the
``war`` file is located in the ``rest-interface/target`` directory.

In the case of Apache Tomcat, copy the ``IESCities.war`` file to the ``webapps`` directory.

Create the ``ies_data`` directory inside the Tomcat directory (e.g
/var/lib/tomcat7) and add permissions for Tomcat on that directory.

Create a ``iescities.properties`` file inside the Tomcat webapp directory. The
file must contain the following properties (the meaning and configuration of
data wrapper related properties is explained in
:ref:`socialapi_configuration`)

  .. code-block:: jproperties

      #################
      #Basic properties
      #################
      #the directory to store dumps and user data
      datadir=ies_data
      #url of the ies cities api
      swaggerurl=http://localhost:8080/IESCities/api
      #the email the messages are sent from (i.e. password change)
      email=a.a@com
      #the smpt server
      host=smtp.somehost.com
      #the smtp server password
      password=pass

      ###########################
      #Social API configuration
      ###########################
      #default values for search parameters if missing in the request to the controller
      default.facebookSources=10
      default.maxPostsNumber=100
      default.radius=1000

      # Facebook client id and client secret
      fb.clientId=
      fb.clientSecret=

      # Twitter consumer key, consumer secret, access token and access token secret
      tw.consumerKey=
      tw.consumerSecret=
      tw.accessToken=
      tw.accessTokenSecret=

.. note::

  Copy the database connectors for `PostgreSQL <https://jdbc.postgresql.org/>`__,
  `MySQL <http://dev.mysql.com/downloads/connector/j/>`__ and `SQLite <https://bitbucket.org/xerial/sqlite-jdbc>`__
  to the Tomcat shared libs directory, as the are required by IES Cities to work
  properly.

Once the servlet is deployed the main web interface should be accessible at

	http://server/IESCities

and the Swagger API description at

	http://server/IESCities/swagger


.. _backup_data:

Backup data
-----------

In order to backup the complete information stored by the platform remember to

	.. warning::
		Backup the database periodically using the ``pg_dump`` utility. See `pg_dump <http://www.postgresql.org/docs/8.4/static/app-pgdump.html>`__
		documentation.

	.. warning::
		Backup the ``ies_data`` directory which will contain the dumped datasets in the
		``ies_data/dump`` directory and the user stored information in the ``ies_data/user``
		directory. User information or dumped data are stored using SQLite databases
		with the following structure: 'id'.db, where ``id`` is the unique identifier of the
		registered dataset.
