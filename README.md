IES Cities Server Documentation
===============================

License
-------

This documentation is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).


Requirements
------------

 * Sphinx http://www.sphinx-doc.org/

Installation
------------

	pip install -r requirements.txt
	

Build
-----

	make html